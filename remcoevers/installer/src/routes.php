<?php

use remcoevers\Installer\InstallerController;

Route::prefix('remcoevers')->group(function() {
    Route::get('installer', function() {
        echo 'This is the installer';
    });

    Route::get('test', [InstallerController::class, 'test']);
});
