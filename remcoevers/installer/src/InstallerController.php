<?php

namespace remcoevers\Installer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class InstallerController extends Controller
{
    public function test()
    {
        return view('installer::demo');
    }
}
