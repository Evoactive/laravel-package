<?php

namespace remcoevers\Installer;

use Illuminate\Support\ServiceProvider;

class InstallerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        // some comment
        $this->app->make('remcoevers\Installer\InstallerController');
        $this->loadViewsFrom(__DIR__ . '/views', 'installer');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        include __DIR__ . '/routes.php';
    }
}
